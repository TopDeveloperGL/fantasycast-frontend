/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
  theme: {
    extend: {
      backgroundImage: {
        // Define your custom background image here
        "main-background": "url('/public/bg.jpg')",
      },
      colors: {
        primary: "#1677ff",
      },
    },
  },
  plugins: [],
};
