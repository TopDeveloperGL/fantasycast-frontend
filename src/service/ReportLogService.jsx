import axios from 'axios'
import { ACCESS_TOKEN_NAME } from '../config';
import { tokens } from '../utils/token';

const apiClient = axios.create({
  baseURL: `${process.env.REACT_APP_SERVER_URL}/api/user/reportlog`,
  headers: {
    'Content-Type': 'application/json',
  },
})

/**
 * Get league data
 */
const fetchReportLog = async () => {
  try {
    apiClient.defaults.headers.common[ACCESS_TOKEN_NAME] = tokens.get()
    const response = await apiClient.get('/list');
    // console.log(response.data.list);
    return response.data.list;
  } catch (error) {
    // Handle any errors from the API call
    throw error;
  }
}

/**
 * Get report data
 */
const fetchMainReport = async (data) => {
  try {
    apiClient.defaults.headers.common[ACCESS_TOKEN_NAME] = tokens.get()
    const response = await apiClient.get('/listbyreport', { params: data });
    // console.log(response.data.list);
    return response.data.list;
  } catch (error) {
    // Handle any errors from the API call
    throw error;
  }
}

const fetchVoices = async () => {
  try {
    apiClient.defaults.headers.common[ACCESS_TOKEN_NAME] = tokens.get()
    const response = await apiClient.get('/voice/list');
    // console.log(response.data);
    return response.data;
  } catch (error) {
    // Handle any errors from the API call
    throw error;
  }
}

const generateAudio = async (data) => {
  try {
    apiClient.defaults.headers.common[ACCESS_TOKEN_NAME] = tokens.get()
    const response = await apiClient.post('/audio/generate', data);
    // console.log(response.data);
    return response.data;
  } catch (error) {
    // Handle any errors from the API call
    throw error;
  }
}


export { fetchMainReport, fetchReportLog, fetchVoices, generateAudio }
