import axios from 'axios'
import { tokens } from '../utils/token'

const apiClient = axios.create({
  baseURL: `${process.env.REACT_APP_SERVER_URL}/api/user/auth`,
  headers: {
    'Content-Type': 'application/json',
  },
})

/**
 * Login with user info
 * @param {Object} data
 */
const login_service = async (data) => {
  try {
    let result = await apiClient.post('/login', data)

    tokens.set(result.data.token)
    return Promise.resolve(result)
  } catch (error) {
    // Handle any errors from the API call
    throw error;
  }
}

/**
 * Register with user info
 * @param {Object} data
 */
const register_service = async (data) => {
  try {
    let result = await apiClient.post('/register', data)
    return result
  } catch (error) {
    // Handle any errors from the API call
    throw error;
  }
}

const logout = () => {
  tokens.remove();
  return true;
};

export { login_service, register_service, logout }
