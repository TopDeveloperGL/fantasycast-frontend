import axios from 'axios'
import { ACCESS_TOKEN_NAME } from '../config';
import { tokens } from '../utils/token';

const apiClient = axios.create({
  baseURL: `${process.env.REACT_APP_SERVER_URL}/api/user/generate`,
  // timeout: 200000,
  headers: {
    'Content-Type': 'application/json',
  },
})

/**
 * Get league data
 */
const runReport = async (url, data) => {
  try {
    apiClient.defaults.headers.common[ACCESS_TOKEN_NAME] = tokens.get()
    const response = await apiClient.post(url, data)
    // console.log(response.data)
    return Promise.resolve(response)
  } catch (error) {
    // Handle any errors from the API call
    return Promise.reject(error)
  }
}

const sendEmail = async (data) => {
  try {
    apiClient.defaults.headers.common[ACCESS_TOKEN_NAME] = tokens.get()
    const response = await apiClient.post("/send/email", data)
    // console.log(response.data)
    return response;
  } catch (error) {
    // Handle any errors from the API call
    throw error;
  }
}

export { runReport, sendEmail }