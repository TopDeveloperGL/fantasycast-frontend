import axios from 'axios'
import { ACCESS_TOKEN_NAME } from '../config';
import { tokens } from '../utils/token';

const apiClient = axios.create({
  baseURL: `${process.env.REACT_APP_SERVER_URL}/api/user/league`,
  headers: {
    'Content-Type': 'application/json',
  },
})

/**
 * Get league data
 */
const fetchLeagues = async (data) => {
  try {
    apiClient.defaults.headers.common[ACCESS_TOKEN_NAME] = tokens.get()
    const response = await apiClient.get('/list');
    // console.log(response.data.list);
    return response.data.list;
  } catch (error) {
    // Handle any errors from the API call
    throw error;
  }
}

export { fetchLeagues }
