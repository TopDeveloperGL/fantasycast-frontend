import axios from 'axios'
import { ACCESS_TOKEN_NAME } from '../config';
import { tokens } from '../utils/token';

const apiClient = axios.create({
  baseURL: `${process.env.REACT_APP_SERVER_URL}/api/user/report`,
  // timeout: 200000,
  headers: {
    'Content-Type': 'application/json',
  },
})

/**
 * Get report data
 */
const fetchReports = async () => {
  try {
    apiClient.defaults.headers.common[ACCESS_TOKEN_NAME] = tokens.get()
    const response = await apiClient.get('/list');
    // console.log(response.data.list);
    return response.data.list;
  } catch (error) {
    // Handle any errors from the API call
    throw error;
  }
}

/**
 * Get league data
 */
const runReport = async (url, data) => {
  try {
    apiClient.defaults.headers.common[ACCESS_TOKEN_NAME] = tokens.get()
    const response = await apiClient.post('/generate/matchup/before', data)
    // console.log(response.data)
    return response.data
  } catch (error) {
    // Handle any errors from the API call
    throw error
  }
}

const getWeekData = async (data) => {
  try {
    apiClient.defaults.headers.common[ACCESS_TOKEN_NAME] = tokens.get()
    const response = await apiClient.post('/week/list', data);
    // console.log(response.data);
    return response.data;
  } catch (error) {
    // Handle any errors from the API call
    throw error;
  }
}

const getTeamData = async (data) => {
  try {
    apiClient.defaults.headers.common[ACCESS_TOKEN_NAME] = tokens.get()
    const response = await apiClient.post('/team/list', data);
    // console.log(response.data);
    return response.data;
  } catch (error) {
    // Handle any errors from the API call
    throw error;
  }
}

const getMatchupData = async (data) => {
  try {
    apiClient.defaults.headers.common[ACCESS_TOKEN_NAME] = tokens.get()
    const response = await apiClient.post('/matchup/list', data);
    // console.log(response.data);
    return response.data;
  } catch (error) {
    // Handle any errors from the API call
    throw error;
  }
}



export { fetchReports, runReport, getTeamData, getMatchupData, getWeekData }
