import axios from 'axios'
import { ACCESS_TOKEN_NAME } from '../config';
import { tokens } from '../utils/token';

const apiClient = axios.create({
  baseURL: `${process.env.REACT_APP_SERVER_URL}/api/user/schedule`,
  // timeout: 200000,
  headers: {
    'Content-Type': 'application/json',
  },
})

const getSchedule = async (data) => {
  try {
    apiClient.defaults.headers.common[ACCESS_TOKEN_NAME] = tokens.get()
    const response = await apiClient.post('/get', data);

    return response.data.schedule;
  } catch (error) {
    // Handle any errors from the API call
    throw error;
  }
}

const saveSchedule = async (data) => {
  try {
    apiClient.defaults.headers.common[ACCESS_TOKEN_NAME] = tokens.get()
    const response = await apiClient.post('/update', data);

    return response.data.schedule;
  } catch (error) {
    // Handle any errors from the API call
    throw error;
  }
}

export { getSchedule, saveSchedule }