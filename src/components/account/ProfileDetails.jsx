import React, { useCallback, useState, useEffect } from 'react';
import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  Divider,
  TextField,
  Unstable_Grid2 as Grid
} from '@mui/material';

import { toast } from 'react-toastify'

import { fetchUser, updateUser } from '../../service/ProfileService';


export const ProfileDetails = () => {
  const [values, setValues] = useState({});

  useEffect(() => {
    (async () => {
      try {
        const user = await fetchUser()
        // console.log(user)
        const { sleeperId, sleeperName, name, email } = user
        setValues({
          sleeperId,
          sleeperName,
          name,
          email,
          password: '',
          confirm: ''
        })
      } catch (error) {
        // Handle the error
        console.error(error);
      }
    })();
  }, [])

  const handleChange = useCallback(
    (event) => {
      setValues((prevState) => ({
        ...prevState,
        [event.target.name]: event.target.value
      }));
    },
    []
  );

  const handleSubmit = (event) => {
    event.preventDefault();
    const { sleeperId, sleeperName, name, email, password, confirm } = values
    if (sleeperId && sleeperName && name && email && password && confirm) {
      (async () => {
        try {
          const res = await updateUser(values)
          if (res.status === 200) {
            toast.success(res.data.message, { autoClose: 2000 })
          }
        } catch (err) {
          // Handle the error
          toast.error(err.response.data.message, { autoClose: 2000 });
        }
      })();
    }
  };

  const isValidEmail = (email) => {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
  }

  const confirmMessage = () => {
    if (values.confirm) {
      if (values.password !== values.confirm) {
        return "Passwords do not match"
      }
    } else {
      return "Please specify the confirm password"
    }
  }

  return (
    <form
      autoComplete="off"
      noValidate
      onSubmit={handleSubmit}
    >
      <Card>
        <CardContent sx={{ pt: 5 }}>
          <Box sx={{ m: -1.5, p: 3 }}>
            <Grid
              container
              spacing={3}
            >
              {/* <Grid
                xs={12}
                md={6}
              >
                <TextField
                  size="small"
                  fullWidth
                  label="Sleeper ID"
                  name="sleeperId"
                  disabled
                  onChange={handleChange}
                  value={values.sleeperId || ''}
                />
              </Grid> */}

              <Grid
                xs={12}
              // md={6}
              >
                <TextField
                  size="small"
                  fullWidth
                  label="Sleeper Name"
                  name="sleeperName"
                  disabled
                  onChange={handleChange}
                  value={values.sleeperName || ''}
                />
              </Grid>

              <Grid
                xs={12}
                md={6}
              >
                <TextField
                  size="small"
                  fullWidth
                  label="Name"
                  name="name"
                  onChange={handleChange}
                  error={!values.name}
                  helperText={!values.name ? "Please specify the name" : ""}
                  required
                  value={values.name || ''}
                />
              </Grid>

              <Grid
                xs={12}
                md={6}
              >
                <TextField
                  size="small"
                  fullWidth
                  label="Email Address"
                  name="email"
                  onChange={handleChange}
                  error={!isValidEmail(values.email)}
                  helperText={!isValidEmail(values.email) ? 'Please specify the email' : ''}
                  required
                  value={values.email || ''}
                />
              </Grid>

              <Grid
                xs={12}
                md={6}
              >
                <TextField
                  size="small"
                  fullWidth
                  label="Password"
                  name="password"
                  type="password"
                  onChange={handleChange}
                  value={values.password || ''}
                  error={!values.password}
                  helperText={!values.password ? "Please specify the password" : ""}
                  required
                />
              </Grid>
              <Grid
                xs={12}
                md={6}
              >
                <TextField
                  size="small"
                  fullWidth
                  label="Confirm Password"
                  name="confirm"
                  type="password"
                  onChange={handleChange}
                  value={values.confirm || ''}
                  error={!values.confirm || values.password !== values.confirm}
                  required
                  helperText={confirmMessage()}
                />
              </Grid>
            </Grid>
          </Box>
        </CardContent>
        <Divider />
        <CardActions sx={{ justifyContent: 'flex-end', pt: 3, pr: 4, pb: 2 }}>
          <Button type='submit' variant="contained" sx={{ backgroundColor: '#1677ff' }}>
            Save details
          </Button>
        </CardActions>
      </Card>
    </form>
  );
};
