export const routes = [
  {
    name: "Your Leagues",
    key: "league",
    path: "/league",
  },
  {
    name: "FantasyCast Profile",
    key: "profile",
    path: "/profile",
  },
  {
    name: "FantasyCast Archive",
    key: "reportlog",
    path: "/reportlog",
  },
];
