import React, { useState, useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { Divider } from '@mui/material'
import { routes } from "../routes"
import { tokens } from '../utils/token';
import { logout } from '../service/AuthService';
import { Typography, Button, IconButton } from '@mui/material'
import { Bars3Icon, XMarkIcon } from "@heroicons/react/24/outline";
import { useMemo } from 'react';

export default function Navbar() {
  const navigate = useNavigate();

  const [openNav, setOpenNav] = useState(false);

  useEffect(() => {
    window.addEventListener(
      "resize",
      () => window.innerWidth >= 960 && setOpenNav(false)
    );
  }, []);

  const navList = useMemo(() => {
    return (
      <ul className="mb-4 mt-2 flex flex-col gap-2 text-inherit lg:mb-0 lg:mt-0 lg:flex-row lg:items-center lg:gap-6">
        {routes.map(({ name, path, icon, href, target }) => (
          <Typography
            key={name}
          // as="li"
          // variant="small"
          // color="inherit"

          >
            {openNav ? (
              <Link
                to={path}
                target={target}
                className="flex items-center gap-1 p-1 font-normal text-black"
                onClick={() => setOpenNav(false)}
              >
                {icon &&
                  React.createElement(icon, {
                    className: "w-[18px] h-[18px] opacity-75 mr-1",
                  })}
                {name}
              </Link>
            ) : (
              <Link
                to={path}
                target={target}
                className="flex items-center gap-1 p-1 font-normal"
                onClick={() => setOpenNav(false)}
              >
                {icon &&
                  React.createElement(icon, {
                    className: "w-[18px] h-[18px] opacity-75 mr-1",
                  })}
                {name}
              </Link>
            )}
          </Typography>
        ))}
      </ul>
    )
  }, [openNav])
  return (
    <nav className="block w-full max-w-screen-2xl rounded-xl bg-transparent text-white shadow-none p-3">
      <div className="container mx-auto flex items-center justify-between text-white">
        <Link to={tokens.get() ? "/league" : "login"}>
          <img
            src="/logo.png"
            alt="model"
            className="max-h-[68px] rounded-none" />
        </Link>
        <div className="hidden lg:block">{navList}</div>
        <div>
          <div className='hidden lg:flex'>
            {!tokens.get() ?
              <Button color="primary" className=' bg-blue-500' onClick={() => navigate("/login")}>
                LOGIN
              </Button> :
              <Button color="primary" className=' bg-blue-500' onClick={() => { logout(); navigate("/login") }}>
                LOGOUT
              </Button>
            }
          </div>
          <div className='lg:hidden'>
            <IconButton
              variant="text"
              size="small"
              color="inherit"
              className="ml-auto text-inherit hover:bg-transparent focus:bg-transparent active:bg-transparent"
              onClick={() => setOpenNav(!openNav)}
            >
              {openNav ? (
                <XMarkIcon strokeWidth={2} className="h-6 w-6" />
              ) : (
                <Bars3Icon strokeWidth={2} className="h-6 w-6 " />
              )}
            </IconButton>
          </div>
        </div>

      </div>
      <div
        className={`block w-full basis-full overflow-hidden rounded-xl bg-slate-400 px-4 pt-2 pb-4 text-blue-gray-900  ${openNav ? 'flex' : 'hidden'}`}
      >
        <div className="container mx-auto">
          {navList}
          {openNav && tokens.get() ?
            <Typography key="logout">
              <Link className="flex items-center gap-1 p-1 font-normal text-black"
                onClick={() => { setOpenNav(false); logout() }} to="/login" >
                Logout
              </Link>
            </Typography> :
            <Typography key="login">
              <Link className="flex items-center gap-1 p-1 font-normal text-black"
                onClick={() => setOpenNav(false)} to="/login">
                Login
              </Link>
            </Typography>
          }

          {/* <Button variant="text" size="small" fullWidth>
            Login
          </Button> */}
        </div>
      </div>
    </nav>
    // <div className='bg-white bg-opacity-10 shadow-lg '>
    //   <div className="mx-auto w-full flex justify-between items-center z-20 py-3 md:py-4 px-2">
    //     <img src="/logo.png" alt="fantasy logo" className="max-w-[50px] cursor-pointer"
    //       onClick={() => {
    //         if (tokens.get())
    //           navigate("/league")
    //         else
    //           navigate("/login")
    //       }} />

    //     {tokens.get() ?
    //       <div className="hidden md:block text-gray-300 text-sm">
    //         {routes.map((navItem) => (
    //           <Link key={navItem.key} className="mx-3 py-3 hover:border-b-2 hover:border-blue-600" to={navItem.path}>{navItem.name}</Link>
    //         ))}
    //       </div> : null}

    //     {tokens.get() ?
    //       <button
    //         className="hidden md:block bg-primary px-7 py-3 rounded-full text-white text-xs hover:brightness-105 focus:outline-none focus:to-blue-700" onClick={() => { logout(); navigate("/login") }}>
    //         Logout
    //       </button> :
    //       <button
    //         className="hidden md:block bg-primary px-7 py-3 rounded-full text-white text-xs hover:brightness-105 focus:outline-none focus:to-blue-700" onClick={() => navigate("/login")}>
    //         Login
    //       </button>
    //     }

    //     <button
    //       onClick={() => setIsOpen(!isOpen)}
    //       className="md:hidden focus:outline-none bg-white"
    //     >
    //       <img
    //         className={`${isOpen && 'hidden'}`}
    //         src="/icon-hamburger.svg"
    //         alt=""
    //       />
    //       <img
    //         className={isOpen ? 'block' : 'hidden'}
    //         src="/icon-close.svg"
    //         alt=""
    //       />
    //     </button>
    //   </div>

    //   <div
    //     key="navbar-modal"
    //     className={`fixed inset-0 z-30 bg-gray-800 bg-opacity-50 ${isOpen ? 'block' : 'hidden'}`}
    //   >
    //     <div key="navbar-list" className="bg-white text-blue-900 flex flex-col text-center m-5 mt-20 py-4 rounded">
    //       {routes.map((navItem) => (
    //         <>
    //           <Link key={navItem.key} className="py-2" to={navItem.path} onClick={() => setIsOpen(!isOpen)}>
    //             {navItem.name}
    //           </Link>
    //           <Divider variant="middle" />
    //         </>
    //       ))}
    //       {tokens.get() ?
    //         <Link key="logout" className="py-2" to="/logout" onClick={() => { logout(); navigate("/login") }}>
    //           Logout
    //         </Link> :
    //         <Link key="login" className="py-2" to="/login" onClick={() => setIsOpen(!isOpen)}>
    //           Login
    //         </Link>}
    //     </div>
    //   </div>
    // </div>
  );
}