import React from 'react'
import Navbar from './Navbar'
import { Outlet } from 'react-router-dom'
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const MainLayout = () => {
  return (
    <>
      <div className="container items-center absolute left-2/4 z-10 mx-auto -translate-x-2/4 p-4">
        <Navbar />
      </div>
      <div className='bg-main-background w-screen h-screen bg-cover bg-center'>
        <div className='flex justify-center items-center pt-36'>
          <Outlet />
        </div>
      </div>
      <ToastContainer />
    </>
  )
}

export default MainLayout
