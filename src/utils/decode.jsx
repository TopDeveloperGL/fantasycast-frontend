import { tokens } from './token'
import jwtDecode from 'jwt-decode'

const tokenDecode = () => {
  // console.log(jwtDecode(tokens.get()))
  if (tokens.get()) {
    return jwtDecode(tokens.get())
  } else {
    return {}
  }
}

export default tokenDecode
