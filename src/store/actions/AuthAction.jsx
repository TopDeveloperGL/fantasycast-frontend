import { SET_CURRENT_USER } from "./actiontypes";

export const setCurrentUser = (data) => (dispatch) => {
  dispatch({
    type: SET_CURRENT_USER,
    payload: data,
  });
};