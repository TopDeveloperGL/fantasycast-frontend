import { SET_CURRENT_LEAGUE } from "./actiontypes";

export const setCurrentLeague = (data) => (dispatch) => {
  dispatch({
    type: SET_CURRENT_LEAGUE,
    payload: data,
  });
};
