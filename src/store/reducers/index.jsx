import { combineReducers } from "redux";
import LeagueReducer from "./LeagueReducer";
import AuthReducer from "./AuthReducer"

export default combineReducers({
  league: LeagueReducer,
  auth: AuthReducer
});