import { SET_CURRENT_USER } from '../actions/actiontypes';

const initialState = {
  role: 1
};

const AuthReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_CURRENT_USER:
      return {
        ...state,
        role: action.payload,
      };
    default:
      return state;
  }
}

export default AuthReducer