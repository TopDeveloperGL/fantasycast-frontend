import { SET_CURRENT_LEAGUE } from '../actions/actiontypes';

const initialState = {
  leagueId: ""
};

const LeagueReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_CURRENT_LEAGUE:
      return {
        ...state,
        leagueId: action.payload,
      };
    default:
      return state;
  }
}

export default LeagueReducer