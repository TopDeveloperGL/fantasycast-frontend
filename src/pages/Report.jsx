import React, { useEffect, useMemo, useState } from 'react';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';

import {
  List,
  ListItem,
  Divider,
  ListItemText,
  Typography,
  Button, Card,
  Box,
  Container,
  FormControl,
  Select,
  MenuItem,
  InputLabel,
  OutlinedInput,
  Checkbox,
  IconButton
} from '@mui/material';

import { RiArrowGoBackFill } from 'react-icons/ri'

import { isEmpty } from 'lodash';

import ScheduleModal from '../components/generate/ScheduleModal';
import RunModal from '../components/generate/RunModal';

import { fetchReports } from '../service/ReportService';
import { getSchedule } from '../service/ScheduleService'
import { fetchVoices } from "../service/ReportLogService"
// import { fetchUser } from '../service/ProfileService';


export default function Report() {
  const [scheduleOpen, setScheduleOpen] = useState(false);
  const [runOpen, setRunOpen] = useState(false)

  const [reportData, setReportData] = useState([])

  const [voiceData, setVoiceData] = useState([]);

  const [selectReport, setSelectReport] = useState({});
  const [userSchedule, setUserSchedule] = useState({});
  const [userVoice, setUserVoice] = useState({})
  const league = useSelector(state => state.league)
  const navigate = useNavigate()

  useEffect(() => {
    (async () => {
      try {
        if (league.leagueId) {
          const reports = await fetchReports()
          // const user = await fetchUser()
          const voices = await fetchVoices();
          const schedules = await getSchedule({ league: league.leagueId })
          let save = voices?.map((item) => {
            return {
              value: item['voice_id'],
              label: item['name'],
              audioURL: item['preview_url'],
            }
          })
          setVoiceData([
            // { value: '0', label: 'Select voice', disabled: true },
            ...save,
          ])
          // console.log(user)
          setUserVoice(schedules?.report_voice)
          // setUserSchedule(user.report_schedule)
          setUserSchedule(schedules?.report_schedule)
          setReportData(reports)
        } else {
          navigate("/league")
        }
      } catch (error) {
        // Handle the error
        console.error(error);
      }
    })();
  }, [])

  return (
    <>
      <div className='bg-white bg-opacity-10 shadow-lg pb-6 max-w-[calc(95vw)]'>
        <Container>
          <Box sx={{
            width: '94%',
            ml: '3%',
            top: -50,
            mt: -3, // margin-top
            mb: 3,
            p: '24px 16px', // padding
            opacity: 1,
            background: 'linear-gradient(195deg, rgb(73, 163, 241), rgb(26,115,232))',
            color: 'rgb(255, 255, 255)',
            borderRadius: '0.5rem',
            boxShadow: 'rgba(0, 0, 0, 0.14) 0rem 0.25rem 1.25rem 0rem, rgba(0, 187, 212, 0.4) 0rem 0.4375rem 0.625rem -0.3125rem',
          }}>
            <Typography variant='h6'>Manage Reports</Typography>
          </Box>

          <Button sx={{ color: 'white' }} size='large' onClick={() => navigate("/league")}>
            Back
          </Button>

          <Button size='large' sx={{ float: 'right', color: "white" }} variant='text' onClick={() => navigate("/archive")}>
            Manage Leagues
          </Button>

          <Card>
            <List sx={{
              width: '100%',
              bgcolor: 'background.paper',
              maxHeight: "calc(100vh - 350px)",
              overflow: "auto"
            }}>
              {reportData?.map(report => (
                <>
                  <ListItem key={report._id} alignItems="flex-start" sx={{ gap: 3 }}>
                    <ListItemText
                      key={report.name}
                      primary={report.description}
                      secondary={
                        <>
                          <Typography
                            sx={{ display: 'inline' }}
                            component="span"
                            variant="body2"
                            color="text.primary"
                          >
                            {report.name}
                          </Typography>
                        </>
                      }
                    />

                    <div key={`action-${report.name}`}>
                      <Button onClick={() => { setSelectReport(report); setRunOpen(true); }}>
                        Run Now
                      </Button>
                      <Button onClick={() => {
                        // setActiveSchedule(!isEmpty(userSchedule) && !isEmpty(userSchedule[report._id]) ? userSchedule[report._id] : [])
                        setSelectReport(report);
                        setScheduleOpen(true);
                      }}>
                        Schedule
                      </Button>
                    </div>


                  </ListItem>
                  <Divider variant="middle" component="li" />
                </>
              ))}
            </List >
          </Card>
        </Container>
      </div>
      <ScheduleModal
        voiceData={voiceData}
        title={selectReport.name}
        reportId={selectReport._id}
        open={scheduleOpen}
        setOpen={setScheduleOpen}
        schedule={selectReport.schedule_list}
        userVoice={userVoice}
        setUserVoice={setUserVoice}
        userSchedule={userSchedule}
        setUserSchedule={setUserSchedule}
      />
      <RunModal voiceData={voiceData} title={selectReport.name} open={runOpen} setOpen={setRunOpen} selectReport={selectReport} />
    </>
  );
}