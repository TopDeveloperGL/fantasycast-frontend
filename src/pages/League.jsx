import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';

// @mui
import {
  Card,
  Table,
  Paper,
  TableRow,
  TableBody,
  TableCell,
  Container,
  Typography,
  Button,
  TableContainer,
  TablePagination,
  Box
} from '@mui/material';
import { DoubleArrowOutlined } from '@mui/icons-material'

import { filter } from 'lodash';

import { TableListHead, TableListToolbar } from '../components/table';

import { setCurrentLeague } from '../store/actions/LeagueAction';

import { fetchLeagues } from '../service/LeaugeService';

// ----------------------------------------------------------------------

const TABLE_HEAD = [
  { id: 'name', label: 'Name', sort: true },
  { id: 'season', label: 'Season', sort: true },
  { id: '', label: 'Action', sort: false }
];

// ----------------------------------------------------------------------

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  if (query) {
    return filter(array, (_user) => _user.name.toLowerCase().indexOf(query.toLowerCase()) !== -1);
  }
  return stabilizedThis.map((el) => el[0]);
}

const Leauge = () => {
  const dispatch = useDispatch()
  const [leagueData, setLeagueData] = useState([])
  const [page, setPage] = useState(0);

  const [order, setOrder] = useState('asc');

  const [selected, setSelected] = useState([]);

  const [orderBy, setOrderBy] = useState('');

  const [filterName, setFilterName] = useState('');

  const [rowsPerPage, setRowsPerPage] = useState(5);

  const navigate = useNavigate()

  useEffect(() => {
    (async () => {
      try {
        const leagues = await fetchLeagues();
        setLeagueData(leagues);
      } catch (error) {
        // Handle the error
        console.error(error);
      }
    })();
  }, [])

  const handleLeague = (e, r) => {
    console.log(e);
    navigate("/report")
  }

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = leagueData.map((n) => n.name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, leagueId) => {
    // console.log(leagueId)
    dispatch(setCurrentLeague(leagueId))
    // navigate("/main");
    // const selectedIndex = selected.indexOf(name);
    // let newSelected = [];
    // if (selectedIndex === -1) {
    //   newSelected = newSelected.concat(selected, name);
    // } else if (selectedIndex === 0) {
    //   newSelected = newSelected.concat(selected.slice(1));
    // } else if (selectedIndex === selected.length - 1) {
    //   newSelected = newSelected.concat(selected.slice(0, -1));
    // } else if (selectedIndex > 0) {
    //   newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
    // }
    // setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setPage(0);
    setRowsPerPage(parseInt(event.target.value, 10));
  };

  const handleFilterByName = (event) => {
    setPage(0);
    setFilterName(event.target.value);
  };

  const emptyRows = page > 0 ? Math.max(0, (1 + page) * rowsPerPage - leagueData.length) : 0;

  const filteredUsers = applySortFilter(leagueData, getComparator(order, orderBy), filterName);

  const isNotFound = !filteredUsers.length && !!filterName;

  return (
    <div className='bg-white bg-opacity-10 shadow-lg pb-6 max-w-[calc(95vw)]'>
      <Container>
        <Box sx={{
          width: '94%',
          ml: '3%',
          top: -50,
          mt: -3, // margin-top
          mb: 3,
          p: '24px 16px', // padding
          opacity: 1,
          background: 'linear-gradient(195deg, rgb(73, 163, 241), rgb(26,115,232))',
          color: 'rgb(255, 255, 255)',
          borderRadius: '0.5rem',
          boxShadow: 'rgba(0, 0, 0, 0.14) 0rem 0.25rem 1.25rem 0rem, rgba(0, 187, 212, 0.4) 0rem 0.4375rem 0.625rem -0.3125rem',
        }}>
          <Typography variant='h6'>Your Leagues</Typography>
        </Box>

        <Card>
          <TableListToolbar numSelected={selected.length} filterName={filterName} onFilterName={handleFilterByName} />

          <TableContainer className='px-6' sx={{ maxHeight: "calc(100vh - 450px)" }}>
            <Table>
              <TableListHead
                order={order}
                orderBy={orderBy}
                headLabel={TABLE_HEAD}
                rowCount={leagueData.length}
                onRequestSort={handleRequestSort}
                onSelectAllClick={handleSelectAllClick}
              />
              <TableBody>
                {filteredUsers.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => {
                  const { league_id, name, season, } = row;

                  return (
                    <TableRow
                      onClick={(event) => handleClick(event, league_id)}
                      hover key={league_id} tabIndex={-1}>
                      <TableCell align='center' component="th" scope="row" padding="none">
                        {name}
                      </TableCell>

                      <TableCell align='center'>{season}</TableCell>

                      <TableCell align='center' sortDirection={false}>
                        <Button sx={{ color: '#1677ff' }} size="large" variant='text' color="inherit" onClick={handleLeague}>
                          Manage Reports
                        </Button>
                      </TableCell>

                    </TableRow>
                  );
                })}
                {emptyRows > 0 && (
                  <TableRow style={{ height: 53 * emptyRows }}>
                    <TableCell colSpan={6} />
                  </TableRow>
                )}
              </TableBody>

              {isNotFound && (
                <TableBody>
                  <TableRow>
                    <TableCell align="center" colSpan={6} sx={{ py: 3 }}>
                      <Paper
                        sx={{
                          textAlign: 'center',
                        }}
                      >
                        <Typography variant="h6" paragraph>
                          Not found
                        </Typography>

                        <Typography variant="body2">
                          No results found for &nbsp;
                          <strong>&quot;{filterName}&quot;</strong>.
                          <br /> Try checking for typos or using complete words.
                        </Typography>
                      </Paper>
                    </TableCell>
                  </TableRow>
                </TableBody>
              )}
            </Table>
          </TableContainer>

          <TablePagination
            className='pl-6 pb-4'
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={leagueData.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Card>
      </Container>
    </div>
  );
}

export default Leauge
