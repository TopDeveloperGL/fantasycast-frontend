import React, { useEffect, useMemo, useState } from 'react'
import {
  Select,
  Typography,
  Space,
  Button,
  Input,
  Row,
  Col,
  Divider,
  Card,
  Modal,
  Cascader,
} from 'antd'
import apiClient from '../service/ReportService'

const { Title } = Typography

const Dashboard = () => {
  const [IDloading, setIDLoading] = useState(false)
  const [promptLoading, setPromptLoading] = useState(false)
  const [reportLoading, setReportLoading] = useState(false)
  const [league, setLeague] = useState(0)
  const [position, setPosition] = useState(0)

  const [team, setTeam] = useState('')
  const [stateRun, setStateRun] = useState(false)

  const [season, setSeason] = useState(0)
  const [leagueList, setLeaugeList] = useState([
    { value: 0, label: 'Select LeagueID', disabled: true },
  ])
  const [week, setWeek] = useState(0)
  const [weeks, setWeeks] = useState([])
  const [weekList, setWeekList] = useState([
    { value: 0, label: 'Week', disabled: true },
  ])
  const [matchup, setMatchup] = useState(0)
  const [matchupList, setMatchupList] = useState([
    { value: 0, label: 'Select Matchup', disabled: true },
  ])

  const [isModalOpen, setIsModalOpen] = useState(false)
  const [report, setReport] = useState(0)

  const [teamOptions, setTeamOptions] = useState([
    { value: '0', label: 'Select team.', disabled: true },
  ])

  const [jsonData, setJsonData] = useState('')
  const [reportData, setReportData] = useState('')
  const [audioLoading, setAudioLoading] = useState(false)

  const [audioURL, setAudioURL] = useState('')
  const [voiceOptions, setVoiceOptions] = useState([])
  const [voiceID, setVoiceID] = useState('')

  const options = [
    {
      value: 0,
      label: 'Select prompt type',
      disabled: true,
    },
    {
      value: 1,
      label: 'Final Matchup Analysis',
    },
    {
      value: 2,
      label: 'Weekly Post Matchup Summary and Analysis',
    },
    {
      value: 3,
      label: 'Power ranking prompt(only one team)',
    },
    {
      value: 4,
      label: 'Post-Draft Analysis',
    },
    {
      value: 5,
      label: 'Transaction Analysis by weekly',
    },
    // {
    //   value: 6,
    //   label: "League Weekly Transaction Analysis",
    // },
    {
      value: 7,
      label: 'Team News Analysis',
    },
  ]

  const get_league_list = async () => {
    await apiClient
      .get('/get_league')
      .then((res) => {
        let list = []
        list = res.data?.map((item) => {
          return {
            value: item.league_id,
            label: item.name,
          }
        })
        setLeague(0)
        setLeaugeList(
          [{ value: 0, label: 'Select LeagueID', disabled: true }].concat(list),
        )
      })
      .catch((err) => console.log(err))
  }

  const get_week_list = async () => {
    await apiClient
      .get(`/get_week/${league}`)
      .then((res) => {
        let list = []
        list = res.data?.map((item) => {
          return {
            value: item.week,
            label: item.week,
          }
        })
        setWeek(0)
        setWeekList([{ value: 0, label: 'Week', disabled: true }].concat(list))
      })
      .catch((err) => console.log(err))
  }

  useEffect(() => {
    if (!IDloading) {
      get_league_list()
    }
  }, [IDloading])

  useEffect(() => {
    get_week_list()
  }, [league])

  useEffect(() => {
    setStateRun(false)
    setMatchupList([{ value: 0, label: 'Select Matchup', disabled: true }])
    setMatchup(0)
    setTeamOptions([{ value: '0', label: 'Select team.', disabled: true }])
    setTeam('0')
    setWeekList([{ value: 0, label: 'Week', disabled: true }])
    setWeek(0)
    setLeague(0)
  }, [position])

  useEffect(() => {
    if (league) {
      if (position === 4) {
        setStateRun(true)
      }
      if ((position === 2 || position === 6) && week) {
        setStateRun(true)
      }
      if (position === 1 && matchup) {
        setStateRun(true)
      }
      if (position === 3 && team.length > 1) {
        setStateRun(true)
      }
      if (position === 5 && weeks.length) {
        setStateRun(true)
      }
      if (position === 7 && team.length > 1) {
        setStateRun(true)
      }
    }
  }, [position, league, week, team, weeks, matchup])

  const buildData = async () => {
    if (season) {
      setStateRun(false)
      setPosition(0)
      setIDLoading(true)
      setJsonData('')
      setReportData('')
      await apiClient
        .post('/buildData', {
          season: season,
        })
        .then(() => {
          setIDLoading(false)
        })
        .catch((err) => {
          console.log(err)
          setIDLoading(false)
        })
    }
  }

  const onSearch = () => {
    setPromptLoading(true)
    if (position) {
      setReport(position)
    }
    if (position === 1) {
      apiClient
        .post('/matchup', {
          league: league,
          week: week,
          matchup: matchup,
        })
        .then((res) => {
          // console.log(res.data)
          setJsonData(
            `${res.data.prefix}\n\n${JSON.stringify(res.data.output)}`,
          )
          setPromptLoading(false)
        })
        .catch((err) => {
          console.log(err)
          setPromptLoading(false)
        })
    }
    if (position === 2) {
      apiClient
        .post('/post_matchup', {
          league: league,
          week: week,
        })
        .then((res) => {
          setJsonData(res.data)
          setPromptLoading(false)
        })
        .catch((err) => {
          console.log(err)
          setPromptLoading(false)
        })
    }
    if (position === 3) {
      apiClient
        .post('/power_ranking', {
          league: league,
          team: team,
        })
        .then((res) => {
          // console.log(res.data)
          setJsonData(
            `${res.data.prefix}\n\n${JSON.stringify(res.data.output)}`,
          )
          setPromptLoading(false)
        })
        .catch((err) => {
          console.log(err)
        })
    }
    if (position === 4) {
      apiClient
        .post('/get_draft', {
          league: league,
          week: week,
        })
        .then((res) => {
          setJsonData(res.data)
          setPromptLoading(false)
        })
        .catch((err) => {
          console.log(err)
        })
    }
    if (position === 5) {
      apiClient
        .post('/get_transaction', {
          league: league,
          weeks: weeks,
          team: team,
        })
        .then((res) => {
          // console.log(res.data)
          setJsonData(
            `${res.data.prefix}\n\n${JSON.stringify(res.data.output)}`,
          )
          setPromptLoading(false)
        })
        .catch((err) => {
          console.log(err)
        })
    }
    if (position === 6) {
      apiClient
        .post('/get_transaction', {
          league: league,
          week: week,
          team: 'All',
        })
        .then((res) => {
          setJsonData(res.data)
          setPromptLoading(false)
        })
        .catch((err) => {
          console.log(err)
        })
    }
    if (position === 7) {
      apiClient
        .post('/team_news', {
          league: league,
          week: week,
          team: team,
        })
        .then((res) => {
          // console.log(res.data)
          setJsonData(
            `${res.data.prefix}\n\n${JSON.stringify(res.data.output)}`,
          )
          setPromptLoading(false)
        })
        .catch((err) => {
          console.log(err)
        })
    }
  }

  const getReport = () => {
    setReportLoading(true)
    apiClient
      .post('/get_completion', {
        text: jsonData,
        position: report,
      })
      .then((res) => {
        setReportData(res.data)
        setReportLoading(false)
      })
      .catch((err) => {
        console.log(err)
        setReportLoading(false)
      })
  }

  useEffect(() => {
    if (week && position === 1) {
      apiClient
        .post('/matchup/list', {
          league: league,
          week: week,
        })
        .then((res) => {
          let matchup_list = Object.entries(res.data)
          let save = []
          matchup_list.forEach((item) => {
            let team_text = item[1].join(' : ')
            save.push({ value: item[0], label: team_text })
          })
          setMatchupList([
            { value: 0, label: 'Select Matchup', disabled: true },
            ...save,
          ])
        })
        .catch((err) => {
          console.log(err)
        })
    }
    if ((position === 3 || position === 5 || position === 7) && league) {
      apiClient
        .post('/get_team', {
          league: league,
        })
        .then((res) => {
          let teamData = Object.entries(res.data)
          let save = []
          teamData.forEach((item) => {
            save.push({ value: item[0], label: item[1] })
          })
          setTeamOptions([
            { value: '0', label: 'Select team1', disabled: true },
            ...save,
          ])
        })
        .catch((err) => {
          console.log(err)
        })
    }
  }, [position, league, week])

  const childComponent = useMemo(() => {
    if (position === 3 || position === 7) {
      return (
        <Select
          style={{ width: 200 }}
          options={teamOptions}
          onChange={(value) => setTeam(value)}
        />
      )
    }
  }, [position, teamOptions])

  const showModal = async () => {
    setIsModalOpen(true)
    let res = await apiClient.get('/voice_list')
    let save = res.data?.map((item) => {
      return {
        value: item['voice_id'],
        label: item['name'],
        audioURL: item['preview_url'],
      }
    })
    setVoiceOptions([
      { value: '0', label: 'Select voice', disabled: true },
      ...save,
    ])
  }

  const handleDownload = () => {
    const a = document.createElement('a')
    let url = `${process.env.REACT_APP_API_URL}/download_audio`
    a.href = url
    a.download = url.split('/').pop()
    a.click()
  }

  const handleAudio = () => {
    setAudioLoading(true)
    apiClient
      .post('/generate_audio', {
        text: reportData,
        voiceID: voiceID,
      })
      .then(() => {
        setAudioLoading(false)
      })
      .catch((err) => {
        console.log(err)
      })
  }

  return (
    <>
      <div style={{ padding: 50 }}>
        <section style={{ textAlign: 'center', marginBottom: 20 }}>
          <Space align="start">
            <img
              style={{ width: 40, height: 40, marginTop: 30 }}
              src="assets/report.png"
              alt="Job Seeker"
            />
            <Title level={2} style={{ marginBottom: 0 }}>
              Fantasy App
            </Title>
          </Space>
        </section>
        <Space style={{ width: '100%', marginTop: 10 }}>
          <Input
            addonBefore="Season:"
            style={{ width: 250 }}
            value={season}
            onChange={(e) => setSeason(e.target.value)}
          />
          <Button type="primary" loading={IDloading} onClick={buildData}>
            Get Data
          </Button>
        </Space>
        <Space style={{ width: '100%', marginTop: 10 }}>
          <Select
            style={{ width: 300 }}
            value={position}
            options={options}
            onChange={(value) => setPosition(value)}
          />
          <Select
            style={{ width: 250 }}
            options={leagueList}
            value={league}
            onChange={(value) => setLeague(value)}
          />
          {position === 1 || position === 2 || position === 6 ? (
            <Select
              style={{ width: 80 }}
              options={weekList}
              value={week}
              onChange={(value) => setWeek(value)}
            />
          ) : (
            ''
          )}
          {position === 1 ? (
            <Select
              style={{ width: 300 }}
              options={matchupList}
              value={matchup}
              onChange={(value) => setMatchup(value)}
            />
          ) : (
            ''
          )}
          {childComponent}
          {position === 5 ? (
            <Cascader
              // style={{
              //   width: 250,
              // }}
              options={weekList}
              onChange={(value) => setWeeks(value)}
              multiple
              maxTagCount="responsive"
            />
          ) : (
            ''
          )}
          <Button
            onClick={onSearch}
            disabled={!stateRun}
            loading={promptLoading}
          >
            Prompt Generate
          </Button>
        </Space>

        <Divider />
        <Row
          gutter={[20, 4]}
          style={{
            display: 'flex',
            justifyContent: 'space-between',
          }}
        >
          <Col span={20}>
            <Card title="Prompt Data">
              <Input.TextArea
                showCount
                style={{
                  height: 250,
                  overflow: 'hidden',
                  marginBottom: 24,
                  fontFamily: 'monospace',
                }}
                onChange={(e) => setJsonData(e.target.value)}
                value={jsonData}
                placeholder="JSON"
              />
            </Card>
          </Col>
          <Col
            span={4}
            style={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              alignContent: 'center',
            }}
          >
            <Button type="primary" onClick={getReport} loading={reportLoading}>
              Report Generate
            </Button>
          </Col>
        </Row>
        <Row
          gutter={[20, 4]}
          style={{
            marginTop: 10,
            display: 'flex',
            justifyContent: 'space-between',
          }}
        >
          <Col span={20}>
            <Card title="Report Data">
              <Input.TextArea
                showCount
                style={{
                  height: 300,
                  overflow: 'hidden',
                  marginBottom: 24,
                }}
                onChange={(e) => setReportData(e.target.value)}
                value={reportData}
                placeholder="Report"
              />
            </Card>
          </Col>
          <Col
            span={4}
            style={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              alignContent: 'center',
            }}
          >
            <Space
              direction="vertical"
              style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                alignContent: 'center',
              }}
            >
              <Button type="primary" onClick={showModal}>
                Generate an audio file
              </Button>
              <Button type="primary" onClick={handleDownload}>
                Download an audio file
              </Button>
            </Space>
          </Col>
        </Row>
        <Modal
          title="Generate an audio file"
          open={isModalOpen}
          footer={[
            <Button
              type="dashed"
              key="cancel"
              onClick={() => setIsModalOpen(false)}
            >
              Cancel
            </Button>,
            <Button
              key="generate"
              type="primary"
              loading={audioLoading}
              onClick={handleAudio}
            >
              Generate
            </Button>,
          ]}
          onCancel={() => setIsModalOpen(false)}
          onOk={() => setIsModalOpen(false)}
        >
          <br />
          <Select
            style={{ width: 200 }}
            options={voiceOptions}
            onChange={(value, option) => {
              setAudioURL(option.audioURL)
              setVoiceID(value)
            }}
            size="large"
          />
          <br />
          <br />
          {audioURL && <audio controls src={audioURL} />}
        </Modal>
      </div>
    </>
  )
}

export default Dashboard
