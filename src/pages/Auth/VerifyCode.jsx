// import React, { useState, useRef } from 'react'
// import { Form, Input, Spin } from 'antd'
// import { toast } from 'react-toastify'
// import ReactCodesInput from 'react-codes-input'
// import 'react-codes-input/lib/react-codes-input.min.css'
// import styled from 'styled-components'

// const Background = styled.div`
//   background: url('bg.jpg');
//   height: 100vh;
//   background-size: cover;
//   background-position: center;

//   &:after {
//     position: absolute;
//     content: '';
//     top: 0;
//     left: 0;
//     height: 100%;
//     width: 100%;
//     background: rgba(0, 0, 0, 0.7);
//   }
// `

// const Content = styled.div`
//   position: absolute;
//   top: 50%;
//   left: 50%;
//   width: 430px;
//   z-index: 999;
//   text-align: left;
//   background: rgba(35, 43, 85, 0.75);
//   transform: translate(-50%, -50%);
//   background-size: cover;
//   margin: 10px 0;
//   padding: 30px 20px;
//   border-top: solid 1px rgba(255, 255, 255, 0.5);
//   border-radius: 5px;
//   box-shadow: 0px 2px 7px rgba(0, 0, 0, 0.2);
//   overflow: hidden;
//   transition: all 0.5s ease;
//   color: #ffffff;
//   font-size: 13px;
// `

// const Header = styled.header`
//   color: white;
//   font-size: 33px;
//   font-weight: 600;
//   margin: 0 0 35px 0;
//   text-align: center;
//   font-family: 'Montserrat', sans-serif;
// `

// const VerifiyCode = () => {
//   const [form] = Form.useForm()
//   const [code, setCode] = useState('')

//   const [result, setResult] = useState()
//   const handleOnChange = (res) => {
//     setResult(res)
//   }

//   const handleChange = (e) => {
//     const value = e.target.value
//     // Allow only numbers and limit the length to 6 digits
//     if (!isNaN(value) && value.length <= 6) {
//       setCode(value)
//     }

//     if (value.length === 6) {
//       console.log(value)
//     }
//   }

//   const onSubmit = (fieldsValue) => {
//     console.log(fieldsValue)
//   }
//   const $passwordWrapperRef = useRef(null)
//   const [password, setPassword] = useState('')

//   return (
//     <Background>
//       <Content>
//         <Header>Verification Code</Header>
//         <Spin spinning={true} style={{ maxWidth: '300px' }}>
//           <ReactCodesInput
//             initialFocus={true}
//             wrapperRef={$passwordWrapperRef}
//             id="password"
//             codeLength={6}
//             type="alphanumeric"
//             hide={false}
//             value={password}
//             onChange={(res) => {
//               setPassword(res)
//               if (res.length === 6) console.log(res)
//             }}
//             customStyleComponent={{
//               textAlign: 'center',
//               maxWidth: '300px',
//               margin: '0 auto',
//             }}
//             customStyleCodeWrapper={{ color: 'white' }}
//           />
//         </Spin>
//         {/* </Form.Item>
//         </Form> */}
//       </Content>
//     </Background>
//   )
// }

// export default VerifiyCode
