import React from 'react'
import { useNavigate } from 'react-router-dom'

import { register_service } from '../../service/AuthService'

import { Form, Input, Button } from 'antd'
import {
  Header,
  FormLabel,
  SignupTab,
  SignupLink,
} from '../../styles/components/auth'
import { toast } from 'react-toastify'
import '../../styles/auth.css'

const Register = () => {
  const [form] = Form.useForm()
  const navigate = useNavigate()

  const onSubmit = (fieldsValue) => {
    register_service(fieldsValue)
      .then((res) => {
        if (res.status === 201) {
          navigate('/login')
        }
      })
      .catch((err) => toast.error(err.response.data.message, { autoClose: 2000 }))
  }

  return (
    <div>
      <div className='w-full max-w-[410px] absolute top-[50%] left-[50%] z-[999] text-left bg-[rgba(35,43,85,0.75)] py-10 transform-gpu translate-x-[-50%] translate-y-[-50%] bg-cover mt-2 mb-2 p-6 border-t border-white border-opacity-50 rounded-md shadow-md overflow-hidden transition-all ease-in-out text-white text-sm'>
        <Header>Register</Header>
        <Form form={form} onFinish={onSubmit} size='small'>

          <Form.Item
            name="sleeperId"
            rules={[
              {
                required: true,
                message: 'Please input your Sleeper UserId!',
              },
            ]}
          >
            <Input placeholder="Sleeper UserId" className="formInput" />
          </Form.Item>

          <Form.Item
            name="name"
            rules={[
              {
                required: true,
                message: 'Please input your name',
              },
            ]}
          >
            <Input placeholder="Name" className="formInput" />
          </Form.Item>

          <Form.Item
            name="email"
            rules={[
              {
                type: 'email',
                message: 'The input is not valid E-mail!',
              },
              {
                required: true,
                message: 'Please input your E-mail!',
              },
            ]}
          >
            <Input placeholder="Email" className="formInput" />
          </Form.Item>

          <Form.Item
            name="password"
            rules={[
              {
                required: true,
                message: 'Please input your password!',
              },
            ]}
          >
            <Input placeholder="Password" type="password" className="formInput" />
          </Form.Item>

          <Form.Item
            name="confirm"
            rules={[
              {
                required: true,
                message: 'Please confirm your password!',
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue('password') === value) {
                    return Promise.resolve()
                  }
                  return Promise.reject(
                    new Error(
                      'The two passwords do not match!',
                    ),
                  )
                },
              }),
            ]}
          >
            <Input placeholder="Confirm Password" type="password" className="formInput" />
          </Form.Item>
          <Form.Item>
            <Button
              size="middle"
              style={{
                width: '100%',
                backgroundColor: 'rgba(16, 89, 255, 1)',
                borderColor: 'rgba(16, 89, 255, 1)',
                color: '#ffffff',
                borderRadius: 20,
                marginTop: 8
              }}
              htmlType="submit"
            >
              REGISTER
            </Button>
          </Form.Item>
        </Form>
        <SignupTab>
          Have an account?
          <SignupLink
            onClick={() => {
              navigate('/login')
            }}
          >
            Login
          </SignupLink>
        </SignupTab>
      </div>
    </div>
  )
}

export default Register
