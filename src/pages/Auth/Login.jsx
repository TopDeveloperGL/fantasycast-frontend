import React from 'react'
import { useNavigate } from 'react-router-dom'

import { login_service } from '../../service/AuthService'
import { tokens } from '../../utils/token'

import { Form, Input, Button } from 'antd'
import { toast } from 'react-toastify'
import {
  Header,
  ForgotPass,
  SignupTab,
  SignupLink,
} from '../../styles/components/auth'
import '../../styles/auth.css'
import { useDispatch } from 'react-redux'
import { setCurrentUser } from '../../store/actions/AuthAction'

const Login = () => {
  const [form] = Form.useForm()
  const navigate = useNavigate()
  const dispatch = useDispatch()

  const onSubmit = (fieldsValue) => {
    login_service(fieldsValue)
      .then((res) => {
        // console.log(res)
        if (res.status === 200) {
          tokens.set(res.data?.token)
          dispatch(setCurrentUser(res.data.role))
          navigate('/league')
        }
      })
      .catch((err) => toast.error(err.response.data.message, { autoClose: 2000 }))
  }

  return (
    <div>
      <div className='w-full max-w-[410px] absolute top-[50%] left-[50%] z-[999] text-left bg-[rgba(35,43,85,0.75)] py-10 transform-gpu translate-x-[-50%] translate-y-[-50%] bg-cover mt-2 mb-2 p-6 border-t border-white border-opacity-50 rounded-md shadow-md overflow-hidden transition-all ease-in-out text-white text-sm'>
        <Header>Login</Header>
        <Form form={form} onFinish={onSubmit}>
          <Form.Item
            name="sleeperId"
            rules={[
              {
                required: true,
                message: 'Please input your sleeprId!',
              },
            ]}
          >
            <Input className="formInput" placeholder="Sleeper UserId" />
          </Form.Item>

          {/* <FormLabel htmlFor="password">Password</FormLabel> */}
          <Form.Item
            name="password"
            rules={[
              {
                required: true,
                message: 'Please input your password!',
              },
            ]}
          >
            <Input placeholder="Password" type="password" className="formInput" />
          </Form.Item>

          <ForgotPass>Forgot Password?</ForgotPass>

          <Form.Item>
            <Button
              size="middle"
              style={{
                width: '100%',
                backgroundColor: 'rgba(16, 89, 255, 1)',
                borderColor: 'rgba(16, 89, 255, 1)',
                color: '#ffffff',
                borderRadius: 20,
                marginTop: 8
              }}
              onMouseOver={({ currentTarget }) =>
                (currentTarget.style.backgroundColor = '#3c79fd')
              }
              onMouseOut={({ currentTarget }) =>
                (currentTarget.style.backgroundColor = 'rgba(16, 89, 255, 1)')
              }
              onMouseUp={({ currentTarget }) =>
                (currentTarget.style.backgroundColor = '#3c79fd')
              }
              onMouseDown={({ currentTarget }) =>
                (currentTarget.style.backgroundColor = 'rgba(16, 89, 255, 1)')
              }
              htmlType="submit"
            >
              LOGIN
            </Button>
          </Form.Item>
        </Form>
        <SignupTab>
          <p>Don't have an account?</p>
          <SignupLink onClick={() => navigate('/register')}>
            Signup Now
          </SignupLink>
        </SignupTab>
      </div>
    </div>
  )
}

export default Login
