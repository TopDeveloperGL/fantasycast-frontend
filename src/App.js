import React, { useState } from "react";
import {
  Routes,
  Route,
  Navigate,
  BrowserRouter,
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import { tokens } from "./utils/token";
import League from "./pages/League";
import Profile from "./pages/Profile";
import ReportLog from "./pages/ReportLog";
import Archive from "./pages/Archive";
import Report from "./pages/Report";
import MainLayout from "./layouts";
import Login from "./pages/Auth/Login";
import Register from "./pages/Auth/Register";
import "./App.css";

const router = createBrowserRouter([
  {
    path: "/",
    element: <MainLayout />,
    children: [
      {
        path: "/league",
        element: <League />,
      },
      {
        path: "/archive",
        element: <Archive />,
      },
      {
        path: "/report",
        element: <Report />,
      },
      {
        path: "/profile",
        element: <Profile />,
      },
      {
        path: "/reportlog",
        element: <ReportLog />,
      },
      {
        path: "/login",
        element: <Login />,
      },
      {
        path: "/register",
        element: <Register />,
      },
      {
        path: "/*",
        element: <Login />,
      },
    ],
  },
]);

function App() {
  return (
    <>
      <RouterProvider router={router} />
    </>
  );
}

export default App;
