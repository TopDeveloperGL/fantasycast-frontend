import styled, { css } from 'styled-components'

const sizes = {
  small: 320,
  medium: 768,
  large: 1024,
  xlarge: 1440,
}

const media = Object.keys(sizes).reduce((accumulator, label) => {
  const emSize = sizes[label] / 16
  accumulator[label] = (...args) => css`
    @media (max-width: ${emSize}em) {
      ${css(...args)}
    }
  `
  return accumulator
}, {})

const Background = styled.div`
  background: url('bg.jpg');
  height: 100vh;
  background-size: cover;
  background-position: center;

  &:after {
    position: absolute;
    content: '';
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
    background: rgba(0, 0, 0, 0.7);
  }
`

const Content = styled.div`
  width: 100%;
  max-width: 410px;
  ${media.xlarge`width: 38%;`}
  ${media.large`width: 38%;`}
  ${media.medium`width: 46%;`}
  ${media.small`width: 80%;`}
  position: absolute;
  top: 50%;
  left: 50%;
  z-index: 999;
  text-align: left;
  background: rgba(35, 43, 85, 0.75);
  transform: translate(-50%, -50%);
  background-size: cover;
  margin: 10px 0;
  padding: 30px 20px;
  border-top: solid 1px rgba(255, 255, 255, 0.5);
  border-radius: 5px;
  box-shadow: 0px 2px 7px rgba(0, 0, 0, 0.2);
  overflow: hidden;
  transition: all 0.5s ease;
  color: #ffffff;
  font-size: 13px;
`

const Header = styled.header`
  color: white;
  font-size: 33px;
  font-weight: 600;
  margin: 0 0 24px 0;
  text-align: center;
  font-family: 'Montserrat', sans-serif;
`

const FormLabel = styled.label`
  margin: 0px 8px;
  font-size: 18px;
  color: white;
`

const ForgotPass = styled.a`
  width: 100%;
  text-align: center;
  /* margin-top: 8px; */
  margin-left: 16px;

  transition: all 0.5s ease;
  color: rgba(255, 255, 255, 0.3);
  font-weight: 400;
  font-size: 13px;
  text-decoration: none;
  font-family: 'Poppins', sans-serif;

  cursor: pointer;
  text-align: left;

  &:hover {
    text-decoration: underline;
  }
`

const SignupTab = styled.div`
  font-size: 15px;
  color: white;
  border-top: solid 1px rgba(255, 255, 255, 0.3);
  padding-top: 16px;
  font-family: 'Poppins', sans-serif;
  text-align: center;
`

const SignupLink = styled.a`
  color: #3498db;
  text-decoration: none;
  margin-left: 3px;
  cursor: pointer;

  &:hover {
    text-decoration: underline;
  }
`

export {
  Background,
  Content,
  Header,
  FormLabel,
  ForgotPass,
  SignupTab,
  SignupLink,
}
